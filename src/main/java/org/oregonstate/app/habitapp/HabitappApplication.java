package org.oregonstate.app.habitapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/*
 * 
Week
3
X    Spring Boot research/study
X    Maven research/study
X    GitLab CI/CD research/study

4
X    Set up Spring Boot “habit”app
X    Set up development environment
X    Develop GitLab CI/CD pipeline to deploy to PWS
X    Implement static JSON responses for front-end integration
X    Progress Video Report

5
X    Set up database tables, including authentication, for persistence.
X    Integrate with the database with JPA
X    Progress Video Report

6
    Implement authentication
    Implement /habits end point with GET/POST methods using JPA
    Contribute to midterm report

7
    Implement /habits end point with PUT/DELETE methods using JPA
    Progress Video Report

8
    Implement /users end point with GET/POST/PUT/DELETE methods using JPA
	Progress Video Report

9
    Fix inevitable defects
    Progress Video Report

10
    Final testing
    Contribute to poster and final report
 */
@SpringBootApplication
public class HabitappApplication 
{
    public static void main(String[] args) 
    {
	SpringApplication.run(HabitappApplication.class, args);
    }
}