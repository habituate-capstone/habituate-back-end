package org.oregonstate.app.habitapp.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import org.oregonstate.app.habitapp.config.FirebaseConfiguration;
import org.oregonstate.app.habitapp.dto.HabitDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.google.api.core.ApiFuture;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class HabitService
{
    private static final Logger LOGGER = Logger.getLogger(HabitService.class.getName());
    
    private static String DB_HABITS_REFERENCE = "habits";
    private static String DB_PARENT_REFERENCE = "/";
    
    public HabitDTO getSingleHabit(String userId, String habitId)
    {
	StringBuilder sb = new StringBuilder();
	sb.append("https://habituate-55f37.firebaseio.com/").append(userId).append("/habits/").append(habitId).append(".json");
	String uri = sb.toString();
	HabitDTO habitDto = ClientBuilder.newClient().target(uri).request(MediaType.APPLICATION_JSON).get(HabitDTO.class);
	return habitDto;
    }  
   
    public void deleteHabit(String habitId, String userId)
    {
	final DatabaseReference habitsRef = FirebaseConfiguration.getFirebaseDatabaseReferenceAtPath(
		userId+"/"+DB_HABITS_REFERENCE+"/"+habitId, 
		DB_PARENT_REFERENCE);
	
	ApiFuture<Void> future = habitsRef.removeValueAsync();
	
	while(!future.isDone())
	{
	    if(future.isCancelled())
	    {
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong deleting habit with id " + habitId);
	    }
	}
    }
    
    public void insertHabit(HabitDTO habitDto)
    {
	habitDto.setHabitId(UUID.randomUUID().toString());
	final DatabaseReference habitsRef = FirebaseConfiguration.getFirebaseDatabaseReferenceAtPath(
		habitDto.getUserId()+"/"+DB_HABITS_REFERENCE+"/"+habitDto.getHabitId(), 
		DB_PARENT_REFERENCE);
	habitsRef.setValueAsync(habitDto);
    }
    
    public void updateHabit(String userId, HabitDTO habitDto, String habitId)
    {
	final DatabaseReference habitsRef = FirebaseConfiguration.getFirebaseDatabaseReferenceAtPath(
		userId+"/"+DB_HABITS_REFERENCE+"/"+habitId,DB_PARENT_REFERENCE);
	Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();
	String habitJson = gson.toJson(habitDto);
	Map<String, Object> updateHabitMap = new HashMap<>(); 
	updateHabitMap = gson.fromJson(habitJson, updateHabitMap.getClass());
	LOGGER.log(Level.INFO, "HABIT MAP : " + updateHabitMap.toString());
	habitsRef.updateChildrenAsync(updateHabitMap);
    }
       
    public List<HabitDTO> getHabitsForUser(String userId) throws InterruptedException
    {
	List<HabitDTO> habitList = new ArrayList<HabitDTO>();
	final DatabaseReference habitsRef = FirebaseConfiguration.getFirebaseDatabaseReferenceAtPath(userId, DB_PARENT_REFERENCE);
	
	ChildEventListener listener = habitsRef.orderByValue().addChildEventListener(new ChildEventListener()
	{
	    @Override
	    public void onChildAdded(DataSnapshot snapshot, String previousChildName)
	    {
		for(DataSnapshot snap : snapshot.getChildren())
		{
		    habitList.add(snap.getValue(HabitDTO.class));
		}
		LOGGER.log(Level.INFO, "HABIT MAP : " + habitList.toString());
		notifyAll();
	    }
	    
	    @Override
	    public void onCancelled(DatabaseError error)
	    {
		// TODO Auto-generated method stub
		
	    }
	    
	    @Override
	    public void onChildChanged(DataSnapshot snapshot, String previousChildName)
	    {
		// TODO Auto-generated method stub
		
	    }
	    
	    @Override
	    public void onChildMoved(DataSnapshot snapshot, String previousChildName)
	    {
		// TODO Auto-generated method stub
		
	    }
	    
	    @Override
	    public void onChildRemoved(DataSnapshot snapshot)
	    {
		// TODO Auto-generated method stub
		
	    }
	});
	int counter = 0;
	do 
	{
	    counter++;
	    Thread.sleep(150);
	    if(counter == 75)
	    {
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Habits requested are not found in the DB");
	    }
	}while(habitList.isEmpty());
	
	habitsRef.addChildEventListener(listener);
	return habitList;
    }
    
    public void updateProgressIfFrequencyIsMet(HabitDTO habitDto) throws ParseException
    {
	switch(habitDto.getFrequency().toLowerCase())
	{
	    case "weekly":
		if(hasAWeekPassed(habitDto))
		{
		    setProgressToZeroAndStartDateStartDatePlusSeven(habitDto);
		    updateHabit(habitDto.getUserId(), habitDto, habitDto.getHabitId());
		}
		break;
	    case "monthly":
		if(hasAMonthPassed(habitDto))
		{
		    setProgressToZeroAndStartDateStartDatePlusThirty(habitDto);
		    updateHabit(habitDto.getUserId(), habitDto, habitDto.getHabitId());
		}
		break;
	    case "daily":
		if(hasADayPassed(habitDto))
		{
		    setProgressToZeroAndStartDateToToday(habitDto);
		    updateHabit(habitDto.getUserId(), habitDto, habitDto.getHabitId());
		}
		break;
	    default:
		break;
	}
	
    }

    protected void setProgressToZeroAndStartDateToToday(HabitDTO habitDto)
    {
	habitDto.setHabitProgress(0);
	habitDto.setStartDate(LocalDate.now().toString());
    }
    
    protected void setProgressToZeroAndStartDateStartDatePlusSeven(HabitDTO habitDto)
    {
	LocalDate habitDay = LocalDate.parse(habitDto.getStartDate());
	habitDto.setHabitProgress(0);
	habitDto.setStartDate(habitDay.plusDays(7).toString());
    }
    
    protected void setProgressToZeroAndStartDateStartDatePlusThirty(HabitDTO habitDto)
    {
	LocalDate habitDay = LocalDate.parse(habitDto.getStartDate());
	habitDto.setHabitProgress(0);
	habitDto.setStartDate(habitDay.plusDays(30).toString());
    }

    protected boolean hasADayPassed(HabitDTO habitDto) throws ParseException
    {
	LocalDate habitDay = LocalDate.parse(habitDto.getStartDate());
	LocalDate today = LocalDate.now();
	System.err.println("Compare to : " + today.compareTo(habitDay));
	if(today.compareTo(habitDay) == 0)
	{
	    return false; 
	}
	else
	{
	    return true;
	}
    }

    protected boolean hasAMonthPassed(HabitDTO habitDto) throws ParseException
    {
	LocalDate habitDay = LocalDate.parse(habitDto.getStartDate());
	LocalDate monthFromHabitDay = habitDay.plusDays(30);
	LocalDate today = LocalDate.now();
	if(today.compareTo(monthFromHabitDay) <= 0)
	{
	    return false; 
	}
	else
	{
	    return true;
	}
    }

    protected boolean hasAWeekPassed(HabitDTO habitDto)
    {
	LocalDate habitDay = LocalDate.parse(habitDto.getStartDate());
	LocalDate weekFromHabitDay = habitDay.plusDays(7);
	LocalDate today = LocalDate.now();
	if(today.compareTo(weekFromHabitDay) <= 0)
	{
	    return false; 
	}
	else
	{
	    return true;
	}
	
    }

    public void checkDatesAndUpdateProgressIfTimeHasElapse(List<HabitDTO> listToReturn) throws ParseException
    {
	for (HabitDTO habitDto : listToReturn)
	{
	    updateProgressIfFrequencyIsMet(habitDto);
	}
    }
}
