package org.oregonstate.app.habitapp.controller;

import org.oregonstate.app.habitapp.config.FirebaseConfiguration;
import org.oregonstate.app.habitapp.dto.UserInformationDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController
{
   
    @RequestMapping(value="/users/save", method = RequestMethod.POST)
    ResponseEntity<String> testAuth(@RequestBody UserInformationDTO userInformationDto)
    {
	return FirebaseConfiguration.addUserToFirebase(userInformationDto);
    }
    
//    @RequestMapping(value="/test-auth2/", method = RequestMethod.GET)
//    ResponseEntity<String> testAuth2(@RequestHeader(value="serverAuthCode") String serverAuthCode) throws IOException
//    {
//	GoogleTokenResponse userToken = FirebaseConfiguration.convertServerAuthToToken(serverAuthCode);
//	Collection<Object> token = userToken.values();
//	for (Object object : token)
//	{
//	    System.err.println("****OBJECTS " + object.toString());
//	}
//	FirebaseToken decodedUserToken = FirebaseConfiguration.verifyUidByIdToken(userToken.getIdToken());
//	FirebaseConfiguration.addUserToFirebase(decodedUserToken);
//	return new ResponseEntity<String>("You're good to go!", HttpStatus.OK);
//    }
    
//  @RequestMapping(value= "/users", method = RequestMethod.GET)
//  ResponseEntity<UserInformationDTO> getUsers()
//  {
//	List<String> habitIds = new ArrayList<String>();
//	habitIds.add("1ac7ec2e-db18-47b7-9a41-9d09190f2a20");
//	habitIds.add("7303bd35-b56b-487b-a3cb-696c3a58818d");
//	habitIds.add("12345d35-a56a-4223-b3ea-123c3a511111");
//	
//	return new ResponseEntity<UserInformationDTO>(new UserInformationDTO(habitIds, "fbaf998d-4525-4ae1-99f8-cc10050f2c7d"), 
//		HttpStatus.OK);
//  }
    
}
