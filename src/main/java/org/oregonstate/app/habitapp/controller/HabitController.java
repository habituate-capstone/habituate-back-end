package org.oregonstate.app.habitapp.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.oregonstate.app.habitapp.dto.HabitDTO;
import org.oregonstate.app.habitapp.service.HabitService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class HabitController
{
    @Inject
    HabitService habitService;
    
    
    @RequestMapping(value= "/habits/{user-id}/{habit-id}", method = RequestMethod.GET)
    ResponseEntity<HabitDTO> getSingleHabit(@PathVariable ("habit-id") String habitId, 
	    @PathVariable("user-id") String userId) throws InterruptedException, ParseException
    {
	HabitDTO habitDto = habitService.getSingleHabit(userId, habitId);
	habitService.updateProgressIfFrequencyIsMet(habitDto);
	return new ResponseEntity<HabitDTO>(habitDto, HttpStatus.OK);
    }
    
    @RequestMapping(value= "/habits/{user-id}/{habit-id}/update", method = RequestMethod.PUT)
    ResponseEntity<String> updateHabit(@PathVariable ("habit-id") String habitId,
	    @RequestBody HabitDTO habitDto, 
	    @PathVariable("user-id") String userId) throws InterruptedException
    {
	if(StringUtils.isNotBlank(habitDto.getHabitId()) || StringUtils.isNotBlank(habitDto.getUserId()))
	{
	    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot update habitId or userId");
	}
	habitService.updateHabit(userId, habitDto, habitId);
	return new ResponseEntity<String>(HttpStatus.OK);
    }
    
    @RequestMapping(value= "/habits/{user-id}/{habit-id}", method = RequestMethod.DELETE)
    ResponseEntity<String> deleteHabit(@PathVariable("habit-id") String habitId, @PathVariable("user-id") String userId) throws InterruptedException
    {
	habitService.deleteHabit(habitId, userId);
	return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @RequestMapping(value= "/habits", method = RequestMethod.POST)
    ResponseEntity<String> postHabits(@Valid @RequestBody (required=true) HabitDTO habitDto) throws URISyntaxException
    {
	habitService.insertHabit(habitDto);
	URI uri = new URIBuilder("/habits/"+habitDto.getUserId()+"/"+habitDto.getHabitId()).build();
	new ResponseEntity<Object>(HttpStatus.CREATED);
	return ResponseEntity.created(uri).build();
    }
    
    @RequestMapping(value= "/habits", method = RequestMethod.GET)
    ResponseEntity<List<HabitDTO>> getHabits(@RequestHeader(value="userId") String userId) throws InterruptedException, ParseException
    {
	List<HabitDTO> habitList = habitService.getHabitsForUser(userId);
	Map<String, HabitDTO> habitMap = new HashMap<String, HabitDTO>();
	for (HabitDTO habitDTO : habitList)
	{
	    habitMap.put(habitDTO.getHabitId(), habitDTO);
	}
	habitList.clear();
	List<HabitDTO> listToReturn = new ArrayList<>();
	listToReturn.addAll(habitMap.values());
	habitService.checkDatesAndUpdateProgressIfTimeHasElapse(listToReturn);
	return new ResponseEntity<List<HabitDTO>>(listToReturn, HttpStatus.OK);
    }
}
