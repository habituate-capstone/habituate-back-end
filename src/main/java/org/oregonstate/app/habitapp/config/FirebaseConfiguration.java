package org.oregonstate.app.habitapp.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.oregonstate.app.habitapp.dto.UserInformationDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.auth.UserRecord.CreateRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

@Configuration
public class FirebaseConfiguration
{
    private static final Logger LOGGER = Logger.getLogger(FirebaseConfiguration.class.getName());
    
    @Bean
    public DatabaseReference firebaseDatabase()
    {
	DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
	return dbRef;
    }
        
    @PostConstruct
    public void connectToFirebase() throws IOException
    {
	PathMatchingResourcePatternResolver pmrpr = new PathMatchingResourcePatternResolver();
	File file = pmrpr.getResource("META-INF/habituate-55f37-firebase-adminsdk-x0ch3-1bd245c96a.json").getFile();
	//FIXME need to implement a solution similar to this (https://stackoverflow.com/questions/20389255/reading-a-resource-file-from-within-jar) to make pws deploy work
	//will figure out how to make sure this doesn't get checked in but is still available for use
	FileInputStream serviceAccount = new FileInputStream(file);
	
	FirebaseOptions options = new FirebaseOptions.Builder()
	  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
	  .setDatabaseUrl("https://habituate-55f37.firebaseio.com")
	  .build();
	
	FirebaseApp.initializeApp(options);
    }
    
    public static DatabaseReference getFirebaseDatabaseReferenceAtPath(String childReference, String referencePath)
    {
	final FirebaseDatabase db = FirebaseDatabase.getInstance();
	DatabaseReference basePathReference = db.getReference(referencePath);
	DatabaseReference specificReference = basePathReference.child(childReference);
	return specificReference;
    }
    
	
    public static ResponseEntity<String> addUserToFirebase(UserInformationDTO userInformationDto)
    {
	if(checkIfUserAlreadyExists(userInformationDto.getUserId()))
	{
	    return new ResponseEntity<String>("User already exists", HttpStatus.NO_CONTENT);
	}
	CreateRequest request = new CreateRequest()
		.setDisabled(false)
		.setUid(userInformationDto.getUserId())
		.setEmail(userInformationDto.getEmail())
		.setDisplayName(userInformationDto.getName());
	try
	{    
	    FirebaseAuth.getInstance().createUser(request);
	} 
	catch (FirebaseAuthException e)
	{
	    LOGGER.log(Level.SEVERE, "Something went wrong creating a user\n" + e.getMessage(), e);
	    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User login information invalid", e);
	}
	return new ResponseEntity<String>("User added", HttpStatus.CREATED);
    }
	
    protected static boolean checkIfUserAlreadyExists(String userId)
    {
	boolean userExists = false;
	try
	{
	    FirebaseAuth.getInstance().getUser(userId);
	    userExists = true;
	} catch (FirebaseAuthException e)
	{
	    LOGGER.log(Level.SEVERE, "Retrieving userId from firebase failed. User may not exist.\n" + e.getMessage(), e);
	}
	return userExists;
    }
    
    /**Connects to Firebase's authentication servers and verifies if the
     * user exists and if the token is valid
     * @param idToken
     * @return
     */
    public static FirebaseToken verifyUidByIdToken(String idToken)
    {
	// idToken comes from the client app (shown above)
	FirebaseToken decodedToken = null;
	try
	{
	    LOGGER.log(Level.INFO, "idToken : " + idToken + "\n");
	    decodedToken = FirebaseAuth.getInstance().verifyIdToken(idToken);
	} catch (FirebaseAuthException e)
	{
	    if(decodedToken != null)
	    {
		LOGGER.log(Level.SEVERE, decodedToken.getEmail() + " " + decodedToken.getUid(), e);
	    }
	    LOGGER.log(Level.SEVERE, "Decoded token failed somwhere : \n" + e.getMessage(), e);
	    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User token provide is not authorized", e);
	}
	catch (IllegalArgumentException e)
	{
	    if(decodedToken != null)
	    {
		LOGGER.log(Level.SEVERE, decodedToken.getEmail() + " " + decodedToken.getUid(), e);
	    }
	    LOGGER.log(Level.SEVERE, "Decoded token failed somwhere : \n" + e.getMessage(), e);
	    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad idToken received", e);
	}
	//return userId
	return decodedToken;
	
    }
    public static GoogleTokenResponse convertServerAuthToToken(String serverAuthCode) throws IOException
    {
	LOGGER.log(Level.INFO, "**********serverAuthCode : " + serverAuthCode);
	if("".equals(serverAuthCode) || null == serverAuthCode)
	{
	    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Server auth code required");
	}
	PathMatchingResourcePatternResolver pmrpr = new PathMatchingResourcePatternResolver();
	File file = pmrpr.getResource("META-INF/client_secret_44837716627-7v29nihp5sn6fc648jre820vt6h6iv88.apps.googleusercontent.com.json").getFile();
	
	//sourced from https://developers.google.com/identity/sign-in/android/offline-access
	GoogleClientSecrets clientSecrets = null;
	try {
	clientSecrets = GoogleClientSecrets.load(JacksonFactory.getDefaultInstance(), new FileReader(file));
	LOGGER.log(Level.INFO, "**********Client Secret Value : " + clientSecrets.toPrettyString());
	LOGGER.log(Level.INFO, "**********Client Secret Details Value : " + clientSecrets.getDetails());
	LOGGER.log(Level.INFO, "**********Client Secret Details Client Id Value : " + clientSecrets.getDetails().getClientId());
	}
	catch (Throwable t)
	{
	    LOGGER.log(Level.SEVERE, "Something went wrong getting client secret" + t.getMessage(), t);
	    throw t;
	}
	GoogleTokenResponse tokenResponse =
	          new GoogleAuthorizationCodeTokenRequest(
	              new NetHttpTransport(),
	              JacksonFactory.getDefaultInstance(),
	              "https://www.googleapis.com/oauth2/v4/token?access_type=offline",
	              clientSecrets.getDetails().getClientId(),
	              clientSecrets.getDetails().getClientSecret(),
	              serverAuthCode,"")
	              .execute();

	return tokenResponse;
    }
}
