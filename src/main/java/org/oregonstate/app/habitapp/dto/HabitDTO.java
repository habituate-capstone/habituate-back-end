package org.oregonstate.app.habitapp.dto;

import javax.validation.constraints.NotNull;
/**
 * @author jsapaugh
 *
 */

public class HabitDTO
{
    private String habitId;
    
    @NotNull(message="Habit name is required")
    private String habitName;
    
    @NotNull(message="Start date is required")
    private String startDate;
    
    @NotNull(message="End date is required")
    private String endDate;
    
    @NotNull(message="Frequency is required")
    private String frequency;
    
    @NotNull(message="Habit goal is required")
    private Integer habitGoal;
    
    @NotNull(message="Time unit is required")
    private String timeUnit;
    
    @NotNull(message="Habit progress is required")
    private Integer habitProgress;
    
    @NotNull(message="Habit category is required")
    private String habitCategory;
    
    @NotNull(message="User id is required")
    private String userId;
    
    public HabitDTO()
    {
	
    }
    
    
    public HabitDTO(String habitId, @NotNull(message = "Habit name is required") String habitName,
            @NotNull(message = "Start date is required") String startDate,
            @NotNull(message = "End date is required") String endDate,
            @NotNull(message = "Frequency is required") String frequency,
            @NotNull(message = "Habit goal is required") int habitGoal,
            @NotNull(message = "Time unit is required") String timeUnit,
            @NotNull(message = "Habit progress is required") int habitProgress,
            @NotNull(message = "Habit category is required") String habitCategory,
            @NotNull(message = "User id is required") String userId)
    {
	super();
	this.habitId = habitId;
	this.habitName = habitName;
	this.startDate = startDate;
	this.endDate = endDate;
	this.frequency = frequency;
	this.habitGoal = habitGoal;
	this.timeUnit = timeUnit;
	this.habitProgress = habitProgress;
	this.habitCategory = habitCategory;
	this.userId = userId;
    }


    public String getFrequency()
    {
        return frequency;
    }

    public void setFrequency(String frequency)
    {
        this.frequency = frequency;
    }

    public int getHabitGoal()
    {
        return habitGoal;
    }

    public void setHabitGoal(int habitGoal)
    {
        this.habitGoal = habitGoal;
    }

    public String getTimeUnit()
    {
        return timeUnit;
    }

    public void setTimeUnit(String timeUnit)
    {
        this.timeUnit = timeUnit;
    }

    public int getHabitProgress()
    {
        return habitProgress;
    }

    public void setHabitProgress(int habitProgress)
    {
        this.habitProgress = habitProgress;
    }

    public String getHabitCategory()
    {
        return habitCategory;
    }

    public void setHabitCategory(String habitCategory)
    {
        this.habitCategory = habitCategory;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }



    public String getHabitName()
    {
	return habitName;
    }

    public void setHabitName(String habitName)
    {
	this.habitName = habitName;
    }

    public String getStartDate()
    {
	return startDate;
    }

    public void setStartDate(String startDate)
    {
	this.startDate = startDate;
    }

    public String getEndDate()
    {
	return endDate;
    }

    public void setEndDate(String endDate)
    {
	this.endDate = endDate;
    }

    public String getHabitId()
    {
	return habitId;
    }

    public void setHabitId(String habitId)
    {
	this.habitId = habitId;
    }

    @Override
    public String toString()
    {
	return "HabitDTO [habitId=" + habitId + ",\n habitName=" + habitName + ",\n startDate=" + startDate + ",\n endDate="
	        + endDate + ",\n frequency=" + frequency + ",\n habitGoal=" + habitGoal + ",\n timeUnit=" + timeUnit
	        + ",\n habitProgress=" + habitProgress + ",\n habitCategory=" + habitCategory + ",\n userId=" + userId + "]";
    }
    
}
