package org.oregonstate.app.habitapp.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.oregonstate.app.habitapp.dto.HabitDTO;

@RunWith(MockitoJUnitRunner.class)
public class HabitServiceTest
{

    @InjectMocks
    HabitService classUnderTestHabitService;
    
    @Test
    public void shouldReturnHabitDtoWithDateAsTodayAndProgressZero() throws ParseException
    {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	HabitDTO habitDto = new HabitDTO();
	classUnderTestHabitService.setProgressToZeroAndStartDateToToday(habitDto);
	assertEquals(0, habitDto.getHabitProgress());
	assertEquals(sdf.format(new Date()), habitDto.getStartDate());
    }
    
    @Test
    public void shouldReturnFalseIfADayHasNotPassed() throws ParseException
    {
	HabitDTO habitDto = new HabitDTO();
	habitDto.setStartDate(LocalDate.now().toString());
	assertFalse(classUnderTestHabitService.hasADayPassed(habitDto));
    }
    
    @Test
    public void shouldReturnTrueIfADayHasPassed() throws ParseException
    {
	HabitDTO habitDto = new HabitDTO();
	habitDto.setStartDate(LocalDate.now().minusDays(3).toString());
	assertTrue(classUnderTestHabitService.hasADayPassed(habitDto));
	habitDto.setStartDate(LocalDate.now().minusDays(1).toString());
	assertTrue(classUnderTestHabitService.hasADayPassed(habitDto));
	habitDto.setStartDate(LocalDate.now().plusDays(1).toString());
	assertTrue(classUnderTestHabitService.hasADayPassed(habitDto));
    }
    
    @Test
    public void shouldReturnFalseIfAWeekHasNotPassed()
    {
	HabitDTO habitDto = new HabitDTO();
	habitDto.setStartDate(LocalDate.now().minusDays(4).toString());
	assertFalse(classUnderTestHabitService.hasAWeekPassed(habitDto));
    }
    
    @Test
    public void shouldReturnTrueIfAWeekHasPassed()
    {
	HabitDTO habitDto = new HabitDTO();
	LocalDate eightDaysFromToday = LocalDate.now().minusDays(8);
	habitDto.setStartDate(eightDaysFromToday.toString());
	assertTrue(classUnderTestHabitService.hasAWeekPassed(habitDto));
    }

    @Test
    public void shouldReturnTrueIfAMonthHasPassed() throws ParseException
    {
	HabitDTO habitDto = new HabitDTO();
	habitDto.setStartDate(LocalDate.now().minusDays(31).toString());
	assertTrue(classUnderTestHabitService.hasAMonthPassed(habitDto));
    }
    
    @Test
    public void shouldReturnFalseIfAMonthHasNotPassed() throws ParseException
    {
	HabitDTO habitDto = new HabitDTO();
	habitDto.setStartDate(LocalDate.now().minusDays(28).toString());
	assertFalse(classUnderTestHabitService.hasAMonthPassed(habitDto));
	habitDto.setStartDate(LocalDate.now().toString());
	assertFalse(classUnderTestHabitService.hasAMonthPassed(habitDto));
	habitDto.setStartDate(LocalDate.now().minusDays(8).toString());
	assertFalse(classUnderTestHabitService.hasAMonthPassed(habitDto));
    }

}
